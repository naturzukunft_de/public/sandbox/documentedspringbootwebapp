package de.naturzukunft.spring.boot.docs.db;

import de.naturzukunft.spring.boot.docs.db.MyEnity.MyEnityIdentifier;

import java.util.UUID;

import org.jmolecules.ddd.types.AggregateRoot;
import org.springframework.data.domain.AbstractAggregateRoot;

import lombok.Getter;

@Getter
public class MyEnity extends AbstractAggregateRoot<MyEnity> implements AggregateRoot<MyEnity, MyEnityIdentifier> {

    private final MyEnityIdentifier id;

    public record MyEnityIdentifier(UUID id) implements org.jmolecules.ddd.types.Identifier {
    }

    public MyEnity() {
        this.id = new MyEnityIdentifier(UUID.randomUUID());
    }
}
