package samplede.naturzukunft.spring.boot.docs;

import de.naturzukunft.spring.boot.docs.SampleApplication;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.modulith.core.ApplicationModules;
import org.springframework.modulith.docs.Documenter;
import org.springframework.modulith.docs.Documenter.CanvasOptions;
import org.springframework.modulith.docs.Documenter.DiagramOptions;
import org.springframework.modulith.docs.Documenter.DiagramOptions.DiagramStyle;

class DocumentationTests {

    @Test
    void createsDocumentation() throws IOException {

        var modules = ApplicationModules.of(SampleApplication.class);

        var canvasOptions = CanvasOptions.defaults()
                .withApiBase("http://localhost:8080/javadoc");

        var diagramOptions = DiagramOptions.defaults()
                .withStyle(DiagramStyle.UML);

        new Documenter(modules)
                .writeDocumentation(diagramOptions, canvasOptions);
    }
}